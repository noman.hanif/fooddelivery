let loader;
let loaderText;
let errorDiv;
let succesDiv;

function startLoader() {

    loader = document.getElementById("loader");
    loaderText = document.getElementById("loaderText");
    errorDiv = document.getElementById("alert-danger");
    succesDiv = document.getElementById('alert-success');

    errorDiv.style.display = "none";
    succesDiv.style.display = 'none';
    loaderText.style.display = "none";
    loader.style.display = "block"


}


function stopLoader() {
    errorDiv.style.display = "none"
    loaderText.style.display = "block";
    loader.style.display = "none";
}

function showError(err) {
    errorDiv.innerHTML = err.message;
    errorDiv.style.display = "block"
    loaderText.style.display = "block";
    loader.style.display = "none"

}


function signup() {

    let username = document.getElementById("signup-username")
    let email = document.getElementById("signup-email")
    let password = document.getElementById("signup-password")
    let country = document.getElementById("signup-country")
    let city = document.getElementById("signup-city")
    let usertype = document.getElementById("signup-usertype")
    
    startLoader();
    firebase.auth().createUserWithEmailAndPassword(email.value, password.value)
        .then((res) => {
            firebase.database().ref(`users/${res.user.uid}`).set({
                username: username.value,
                email: email.value,
                password: password.value,
                country: country.value,
                city: city.value,
                usertype: usertype.value
            })
                .then(() => {

                    succesDiv.innerHTML = "User register succesfully";
                    succesDiv.style.display = 'block'
                    username.value = "";
                    email.value = "";
                    password.value = "";
                    country.value = "";
                    city.value = "";
                    stopLoader();
                    setTimeout(() => {
                        window.location.replace("login.html")
                    }, 1000)
                })
                .catch((err) => {
                    showError(err);
                })
        })
        .catch((err) => {

            showError(err);

        })
}
function login() {
    debugger
    let email = document.getElementById("signin-email");
    let password = document.getElementById("signin-password");
    let usertype =document.getElementById("signin-usertype")
    startLoader();

    firebase.auth().signInWithEmailAndPassword(email.value, password.value)
        .then((res) => {
            let succesDiv = document.getElementById('alert-success');
            succesDiv.innerHTML = "User login succesfully";
            succesDiv.style.display = 'block'
            email.value = "";
            password.value = ""
            stopLoader();
if(usertype.value === "resturant")
{
    setTimeout(() => {
                window.location = "resturant.html"
            }, 1000)
}
else if(usertype.value === "User")
{
    setTimeout(() => {
        window.location = "index.html"
    }, 1000)
}
            

        })
        .catch((err) => {
            showError(err);
        })

}
